# Search
import requests

url = "http://localhost:9200/ip_address/_search?q=ip_address:8.8.8.8"
resp = requests.get(url=url)

foo = {
    'name': 1,
    'report': 2,
    'is_error': res
}

resp.ok

# # Store data 
# import requests

# data = {'foo':'bar'}

# url = "http://localhost:9200/ip_address"
# resp = requests.post(url=url, data=data)
# print(resp.json())

# # Using elasticsearch
# from datetime import datetime
# from elasticsearch import Elasticsearch
# es = Elasticsearch()

# # Creating our data
# doc = {
#     'author': 'kimchy',
#     'text': 'Elasticsearch: cool. bonsai cool.',
#     'timestamp': datetime.now(),
# }

# # Storing the data
# res = es.index(index="test-index", id=1, document=doc)
# print(res['result'])

# # Searches by id
# res = es.get(index="test-index", id=1)
# print(res['_source'])

# es.indices.refresh(index="test-index")

# # Search by query
# res = es.search(index="test-index", query={"match_all": {}})
# print("Got %d Hits:" % res['hits']['total']['value'])
# for hit in res['hits']['hits']:
#     print("%(timestamp)s %(author)s: %(text)s" % hit["_source"])

