import os
import fastapi
from elasticsearch import AsyncElasticsearch

es = AsyncElasticsearch(os.environ["ELASTICSEARCH_HOSTS"])
router = fastapi.APIRouter()

async def es_ip_report_search(ip: str):
    # Create index if there is none
    if not (await es.indices.exists(index='ip_address')):
        await es.indices.create(index='ip_address')

    query = {"query": {"term": {'ip_address': ip}}}

    # Search index for given word
    search_response = await es.search(index='ip_address', body=query)

    # If the word exist, return query, if not add it
    if search_response['hits']['total']['value'] > 0:
        return search_response
    else:
        return False


async def es_ip_report_add(report: dict):
        insert_response = await es.index(index='ip_address', body=report)

        if insert_response['_shards']['successful'] == 1:
            return True
        else:
            return False

async def es_report_search(index: str, object: str):
    # Create index if there is none
    if not (await es.indices.exists(index=index)):
        await es.indices.create(index=index)

    query = {"query": {"term": {index: object}}}

    # Search index for given word
    search_response = await es.search(index=index, body=query)

    # If the word exist, return query, if not add it
    if search_response['hits']['total']['value'] > 0:
        return search_response
    else:
        return False


async def es_report_add(index, report: dict):
        insert_response = await es.index(index=index, body=report)

        if insert_response['_shards']['successful'] == 1:
            return True
        else:
            return False
