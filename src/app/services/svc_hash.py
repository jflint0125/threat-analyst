from typing import Optional
from datetime import datetime
from time import sleep
import httpx

vt_api_key: Optional[str] = None

async def get_async_report_vt(hash: str) -> dict:

   # Main URL used to run requests
    root_url = "https://www.virustotal.com/api/v3"

    # Full URL used for requests
    url = root_url + "/files/" + hash

    # API key set
    headers = {'x-apikey': vt_api_key}

    async with httpx.AsyncClient() as client:
        resp = await client.get(url, headers=headers)
    
    data = resp.json()

    # Threat data pulled from API and put into categories
    malicious = data['data']['attributes']['total_votes']['malicious']

    threat = None
    if malicious >= 5:
        threat = 3
    elif malicious >= 3 and malicious <= 5:
        threat = 2
    elif (malicious <= 3 and malicious > 0):
        threat = 1
    else:
        threat = 0

    # TODO: Need to figure out what I want to extract from VT
    threat_data = None

    results = {
        'is_error': resp.is_error,
        'simple': {
            'name': 'VirusTotal',
            'report':  threat 
        },
        'brief': {
            'name': 'VirusTotal',
            'types': ['threat', 'informational'],
            'report': threat_data
        },
        'detailed': {
            'name': 'VirusTotal',
            'report': data
        }
    }

    return results

async def get_async_hash_reports(hash: str) -> dict:
    
    results = []
    report = {
        'score': None,
        'hash': hash,
        'timestamp': datetime.now(),
        'success': None,
        'simple': [],
        'brief': [],
        'detailed': []}

    results.append(await get_async_report_vt(hash))

    scores = []
    counter = 0
    report_count = len(results)
    for result in results:
        report['simple'].append(result['simple'])
        report['brief'].append(result['brief'])
        report['detailed'].append(result['detailed'])
        scores.append(result['simple']['report'])
        if not result['is_error']:
            counter +=1 

    report['score'] = max(scores)
    report['success'] = f'{counter}/{report_count}'

    return report