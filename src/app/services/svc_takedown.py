from typing import Optional

rapid_api_key: Optional[str] = None
vt_api_key: Optional[str] = None
abuse_api_key: Optional[str] = None


# Abuse IP DB (IP Only)
async def report_to_abuseipdb(ip: str) -> dict:
    return {'msg': 'Ran AbuseIPDB'}

# Virus Total (Domain or IP)
async def report_to_vt(ip: str) -> dict:
    return {'msg': 'Ran VirusTotal'}

# PhishTank (URLS, but you can submit a root URL)
async def report_to_phishtank(ip: str) -> dict:
    return {'msg': 'Ran Phishtank'}

# Scumware (Domain, Host, or URL)
async def report_to_scumware(ip: str) -> dict:
    return {'msg': 'Ran Scumware'}

# Google Safebrowsing (URL)
async def report_to_safe_browsing(ip: str) -> dict:
    return {'msg': 'Ran Safe Browsing'}

###########################################################################

# Run all reports based on input
async def run_reports_ip(ip: str) -> dict:
    return {'msg': 'Ran Takedown Report'}

async def run_reports_domain(domain: str) -> dict:
    return {'msg': 'Ran Takedown Report'}
