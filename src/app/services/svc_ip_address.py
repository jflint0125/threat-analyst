from typing import Optional
from datetime import datetime
from time import sleep
import httpx

from app.services import svc_elasticsearch

rapid_api_key: Optional[str] = None
vt_api_key: Optional[str] = None
abuse_api_key: Optional[str] = None
ip = "8.8.8.8"


async def get_async_report_vt(ip: str) -> dict:
    # Main URL used to run requests
    root_url = "https://www.virustotal.com/api/v3"

    # Full URL used for requests
    ip_url = root_url + "/ip_addresses/" + ip

    # API key set
    headers = {'x-apikey': vt_api_key}

    # Old method to get data
    # r = requests.get(ip_url, headers=headers)

    async with httpx.AsyncClient() as client:
        resp = await client.get(ip_url, headers=headers)
    
    data = resp.json()

    # Threat data pulled from API and put into categories
    analysis = data['data']['attributes']['last_analysis_stats']
    harmless = analysis['harmless']
    malicious = analysis['malicious']
    suspicious = analysis['suspicious']
    undetected = analysis['undetected']
    timeout = analysis['timeout']

    threat_data = {
        'harmless': + harmless,
        'malicious': + malicious,
        'suspicious': + suspicious,
        'undetected': + undetected,
        'timeout': + timeout
    }

    threat = None
    if malicious >= 5 or suspicious >= 30:
        threat = 3
    elif malicious >= 3 and malicious <= 5 or suspicious >= 15:
        threat = 2
    elif (malicious <= 3 and malicious > 0) or suspicious < 5:
        threat = 1
    else:
        threat = 0

    results = {
        'is_error': resp.is_error,
        'simple': {
            'name': 'VirusTotal',
            'report':  threat 
        },
        'brief': {
            'name': 'VirusTotal',
            'types': ['threat', 'informational'],
            'report': threat_data
        },
        'detailed': {
            'name': 'VirusTotal',
            'report': data
        }
    }

    return results


async def get_async_report_whois(ip: str) ->dict:

    url = 'https://zozor54-whois-lookup-v1.p.rapidapi.com/getDomainsFromIp'
    params = {'ip': ip}

    headers = {
        'x-rapidapi-host': "zozor54-whois-lookup-v1.p.rapidapi.com",
        'x-rapidapi-key': vt_api_key
    }

    async with httpx.AsyncClient() as client:
        resp = await client.get(url, headers=headers, params=params)

    data = resp.json()
    return data


async def get_async_report_abuseipdb(ip: str) -> dict:
    url = 'https://api.abuseipdb.com/api/v2/check'

    params = {
        'ipAddress': ip,
        'maxAgeInDays': '120'
    }

    headers = {
        'Accept': 'application/json',
        'Key': abuse_api_key
    }

    async with httpx.AsyncClient() as client:
        resp = await client.get(url, headers=headers, params=params)
        resp.raise_for_status()

    abuseData = resp.json()

    abuseDetail = {
        'Abuse Confidence Score': abuseData['data']['abuseConfidenceScore'],
        'Country Code': abuseData['data']['countryCode'],
        'Type': abuseData['data']['usageType'],
        'ISP': abuseData['data']['isp'],
        'Domain': abuseData['data']['domain'],
        'Total Reports': abuseData['data']['totalReports']
    }

    confidenceScore = int(abuseDetail['Abuse Confidence Score'])

    score = None
    if confidenceScore > 0:
        score = 3
    else:
        score = 0
    
    results = {
        'is_error': resp.is_error,
        'simple': {
            'name': 'AbuseIPDB',
            'report':  score 
        },
        'brief': {
            'name': 'AbuseIPDB',
            'types': ['threat', 'informational'],
            'report': abuseDetail
        },
        'detailed': {
            'name': 'AbuseIPDB',
            'report': abuseData
        }
    }
    return results


async def get_async_ip_reports(ip: str):
    
    es = svc_elasticsearch
    report = await es.es_ip_report_search(ip)
    if report:
        return report
    else:
        results = []
        report = {
            'score': None,
            'ip_address': ip,
            'timestamp': datetime.now(),
            'success': None,
            'simple': [],
            'brief': [],
            'detailed': []}

        results.append(await get_async_report_vt(ip))
        results.append(await get_async_report_abuseipdb(ip))

        scores = []
        counter = 0
        report_count = len(results)
        for result in results:
            report['simple'].append(result['simple'])
            report['brief'].append(result['brief'])
            report['detailed'].append(result['detailed'])
            scores.append(result['simple']['report'])
            if not result['is_error']:
                counter +=1 

        report['score'] = max(scores)
        report['success'] = f'{counter}/{report_count}'

        if(await es.es_ip_report_add(report)):
            sleep(1)
            es_report = await es.es_ip_report_search(ip)
            return es_report
        else:
            return {'msg': 'failed to add report to es'}


# In the end, this is what I would like the sample report to look like
mysample = {
    'score': 'bad',
    'ip_address': '1.1.1.1',
    'success': '2/2',
    'simple': [
        {
            'name': 'virus_total', 
            'report': 'bad'
        },
        {
            'name': 'abuse_ip_db', 
            'report': 'bad'
        },
    ],
    'brief': [
        {
            'name': 'virus_total',
            'types': ['threat', 'informational'],   
            'report': {} # Entire Brief Here
        },
        {
            'name': 'abuse_ipdb',
            'types': ['threat'], 
            'report': {} # Entire Brief Here
        },
    ],
    'detailed': [
        {
            'name': 'virus_total', 
            'report': {} # Entire Report Here
        },
        {
            'name': 'virus_total', 
            'report': {} # Entire Report Here
        },
    ]    
}