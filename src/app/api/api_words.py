import datetime
import os
import fastapi
from time import sleep
from elasticsearch import AsyncElasticsearch

es = AsyncElasticsearch(os.environ["ELASTICSEARCH_HOSTS"])
router = fastapi.APIRouter()


@router.get("/test/word/search/{word}")
async def word_search(word):
    # Create index if there is none
    if not (await es.indices.exists(index="words")):
        await es.indices.create(index="words")

    # Search index for given word
    search_response = await es.search(
        index="words", body={"query": {"multi_match": {"query": word}}}
    )

    # If the word exist, return query, if not add it
    if search_response['hits']['total']['value'] > 0:
        return search_response
    else:
        doc = {
            'datetime': datetime.datetime.now(),
            'word': word
        }
            
        insert_response = await es.index(index="words", body=doc)

        if insert_response['_shards']['successful'] == 1:
            sleep(1)
            search_response = await es.search(
                index="words", body={"query": {"multi_match": {"query": word}}}
            )
            return search_response
        else:
            return insert_response
