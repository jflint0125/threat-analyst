import fastapi
from app.services import svc_takedown

router = fastapi.APIRouter()

@router.get('/apiv1/takedown/ip_address/{ip}')
async def takedown_ip(ip: str):
    results = await svc_takedown.run_reports_ip(ip)
    return results

