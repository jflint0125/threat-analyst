import fastapi
from app.services import svc_ip_address

router = fastapi.APIRouter()

@router.get('/apiv1/threat/ip_address/analyze/{ip}')
async def ip_address(ip: str):
    reports = await svc_ip_address.get_async_ip_reports(ip)
    return reports


