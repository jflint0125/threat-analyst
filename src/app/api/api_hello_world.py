import fastapi
from app.services import svc_hello_world

router = fastapi.APIRouter()

@router.get('/apiv1/tests/helloworld')
async def hello():
    results = await svc_hello_world.hello()
    return results
