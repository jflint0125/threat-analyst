import aiohttp
import datetime
import os
import fastapi
from fastapi.encoders import jsonable_encoder
from elasticsearch import AsyncElasticsearch, NotFoundError
from elasticsearch.helpers import async_streaming_bulk

es = AsyncElasticsearch(os.environ["ELASTICSEARCH_HOSTS"])
router = fastapi.APIRouter()


@router.on_event("shutdown")
async def app_shutdown():
    await es.close()


async def download_games_db():
    async with aiohttp.ClientSession() as http:
        url = "https://cdn.thegamesdb.net/json/database-latest.json"
        resp = await http.request("GET", url)
        for game in (await resp.json())["data"]["games"][:100]:
            yield game


@router.get("/test/health")
async def index():
    return await es.cluster.health()


@router.get("/test/games/ingest")
async def ingest():
    if not (await es.indices.exists(index="games")):
        await es.indices.create(index="games")

    async for _ in async_streaming_bulk(
        client=es, index="games", actions=download_games_db()
    ):
        pass

    return {"status": "ok"}


@router.get("/test/games/search/{query}")
async def search(query):
    return await es.search(
        index="games", body={"query": {"multi_match": {"query": query}}}
    )


@router.get("/test/games/delete")
async def delete():
    return await es.delete_by_query(index="games", body={"query": {"match_all": {}}})


@router.get("/test/games/delete/{id}")
async def delete_id(id):
    try:
        return await es.delete(index="games", id=id)
    except NotFoundError as e:
        return e.info, 404


@router.get("/test/games/update")
async def update():
    response = []
    docs = await es.search(
        index="games", body={"query": {"multi_match": {"query": ""}}}
    )
    now = datetime.datetime.utcnow()
    for doc in docs["hits"]["hits"]:
        response.append(
            await es.update(
                index="games", id=doc["_id"], body={"doc": {"modified": now}}
            )
        )

    return jsonable_encoder(response)


@router.get("/test/games/error")
async def error():
    try:
        await es.delete(index="games", id="somerandomid")
    except NotFoundError as e:
        return e.info


@router.get("/test/games/doc/{id}")
async def get_doc(id):
    return await es.get(index="games", id=id)
