import fastapi
from app.services import svc_hash

router = fastapi.APIRouter()

@router.get('/apiv1/threat/hash/analyze/{hash}')
async def hash(hash: str):
    reports = await svc_hash.get_async_hash_reports(hash)
    return reports