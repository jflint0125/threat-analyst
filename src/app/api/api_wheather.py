from typing import Optional, List

import fastapi
from fastapi import Depends

from app.models.location import Location
from app.models.reports import Report, ReportSubmittal
from app.models.validation_error import ValidationError
from app.services import svc_openweather, svc_report

router = fastapi.APIRouter()


@router.get('/apiv1/weather/{city}')
async def weather(loc: Location = Depends(), units: Optional[str] = 'metric'):
    try:
        return await svc_openweather.get_report_async(loc.city, loc.state, loc.country, units)
    except ValidationError as ve:
        return fastapi.Response(content=ve.error_msg, status_code=ve.status_code)
    except Exception as x:
        return fastapi.Response(content=str(x), status_code=500)


@router.get('/apiv1/reports', name='all_reports', response_model=List[Report])
async def reports_get() -> List[Report]:
    # await report_service.add_report("A", Location(city="Portland"))
    # await report_service.add_report("B", Location(city="NYC"))
    return await svc_report.get_reports()


@router.post('/apiv1/reports', name='add_report', status_code=201, response_model=Report)
async def reports_post(report_submittal: ReportSubmittal) -> Report:
    d = report_submittal.description
    loc = report_submittal.location

    return await svc_report.add_report(d, loc)