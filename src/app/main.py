# Licensed to Elasticsearch B.V under one or more agreements.
# Elasticsearch B.V licenses this file to you under the Apache 2.0 License.
# See the LICENSE file in the project root for more information

import aiohttp
import datetime
import os
from time import sleep
import uvicorn
from fastapi import FastAPI
from fastapi.encoders import jsonable_encoder
from starlette.staticfiles import StaticFiles
from elasticsearch import AsyncElasticsearch, NotFoundError
from elasticsearch.helpers import async_streaming_bulk
from elasticapm.contrib.starlette import ElasticAPM, make_apm_client


from app.api import api_hello_world, api_ip_address, api_takedown, api_games, api_words, api_wheather, api_hash
from app.services import svc_ip_address, svc_openweather, svc_hash
from app.views import home, playground

es = AsyncElasticsearch(os.environ["ELASTICSEARCH_HOSTS"])
app = FastAPI()

# apm = make_apm_client(
#     {"SERVICE_NAME": "fastapi-app", "SERVER_URL": "http://apm-server:8200"})
# app.add_middleware(ElasticAPM, client=apm)

def configure_api_keys():
    svc_ip_address.rapid_api_key = os.environ.get('API_KEY_RAPID')
    svc_ip_address.vt_api_key = os.environ.get('API_KEY_VT')
    svc_ip_address.abuse_api_key = os.environ.get('API_KEY_ABUSEIPDB')
    svc_openweather.api_key_openweather = os.environ.get('API_KEY_OPENWEATHER')
    svc_hash.vt_api_key = os.environ.get('API_KEY_VT')


def configure_routing():
    app.mount('/static', StaticFiles(directory='app/static'), name='static')
    app.include_router(api_hello_world.router)
    app.include_router(api_ip_address.router)
    app.include_router(api_takedown.router)
    app.include_router(api_words.router)
    app.include_router(api_wheather.router)
    app.include_router(home.router)
    app.include_router(playground.router)
    app.include_router(api_hash.router) 
    # app.include_router(api_games.router)


def configure():
    configure_routing()
    configure_api_keys()


if __name__ == '__main__':
    configure()
    # uvicorn was updated, and it's type definitions don't match FastAPI,
    # but the server and code still work fine. So ignore PyCharm's warning:
    # noinspection PyTypeChecker
    uvicorn.run(app, port=9292, host='127.0.0.1')
else:
    configure()