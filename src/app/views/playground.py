import fastapi
from starlette.requests import Request
from starlette.responses import RedirectResponse
from starlette.templating import Jinja2Templates

templates = Jinja2Templates('app/templates')
router = fastapi.APIRouter()


@router.get('/playground', include_in_schema=False)
def playground(request: Request):
    return templates.TemplateResponse('playground/index.html', {'request': request})
