# Threat Analysis



## Getting started

This project uses docker and uses env files to place variables and secrets into the docker containers. 

To get started copy the dockerfiles/app.env.sample to dockerfiles/app.env, then add the appropriate API Keys.

```
git clone git@gitlab.com:kjflint/threat_analysis.git
cp dockerfiles/app.env.sample dockerfiles/app.env
nano dockerfiles/app.env
```

Add your API keys. 

Once the appropriate keys are added to the enviroment file, you can use docker compose to get started.

```
docker-compose up
```

## MD Sample Formatting

Linking from within this README [Use the template at the bottom](#editing-this-readme)!

Making todo lists:
- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:966cb46c22461f329b549c29ad96a32a?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:966cb46c22461f329b549c29ad96a32a?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:966cb46c22461f329b549c29ad96a32a?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:966cb46c22461f329b549c29ad96a32a?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)
- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:966cb46c22461f329b549c29ad96a32a?https://docs.gitlab.com/ee/user/project/integrations/)


Links: [Create](https://gitlab.com/-/experiment/new_project_readme_content:966cb46c22461f329b549c29ad96a32a?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) 

Adding code snippets 

```
cd existing_repo
git remote add origin https://gitlab.com/kjflint/threat_analysis.git
git branch -M main
git push -uf origin main
```

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:966cb46c22461f329b549c29ad96a32a?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:966cb46c22461f329b549c29ad96a32a?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:966cb46c22461f329b549c29ad96a32a?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:966cb46c22461f329b549c29ad96a32a?https://docs.gitlab.com/ee/user/clusters/agent/)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:966cb46c22461f329b549c29ad96a32a?https://www.makeareadme.com/) for this template.

